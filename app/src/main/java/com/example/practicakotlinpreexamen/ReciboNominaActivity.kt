package com.example.practicakotlinpreexamen
//terminado
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity


class ReciboNominaActivity : AppCompatActivity() {
    private lateinit var txtNumRecibo: EditText
    private lateinit var txtNombre: EditText
    private lateinit var txtHorasT: EditText
    private lateinit var txtHorasE: EditText
    private lateinit var btnCalcular: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnRegresar: Button
    private lateinit var radioGroup: RadioGroup
    private lateinit var radioAuxiliar: RadioButton
    private lateinit var radioAlbañil: RadioButton
    private lateinit var radioIngObra: RadioButton
    private lateinit var lblUsuario: TextView
    private lateinit var txtSubtotal: TextView
    private lateinit var txtImpuesto: TextView
    private lateinit var txtTotal: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recibo_nomina)

        // Asignar las referencias a los elementos de la interfaz
        txtNumRecibo = findViewById(R.id.txtNumRecibo)
        txtNombre = findViewById(R.id.txtNombre)
        txtHorasT = findViewById(R.id.txtHorasT)
        txtHorasE = findViewById(R.id.txtHorasE)
        btnCalcular = findViewById(R.id.btnCalcular)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)
        radioGroup = findViewById(R.id.radioGroup)
        radioAuxiliar = findViewById(R.id.radioAuxiliar)
        radioAlbañil = findViewById(R.id.radioAlbañil)
        radioIngObra = findViewById(R.id.radioIngObra)
        lblUsuario = findViewById(R.id.lblUsuario)
        txtSubtotal = findViewById(R.id.txtSubtotal)
        txtImpuesto = findViewById(R.id.txtImpuesto)
        txtTotal = findViewById(R.id.txtTotal)

        // Obtener el usuario de la actividad anterior
        val bundle = intent.extras
        if (bundle != null) {
            val usuario = bundle.getString("user")
            lblUsuario.text = "Bienvenido $usuario"
        }

        // Botón para calcular
        btnCalcular.setOnClickListener {
            if (validarCampos()) {
                calcularNomina()
            } else {
                Toast.makeText(
                    this@ReciboNominaActivity,
                    "Por favor, completa todos los campos",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        // Botón de limpiar
        btnLimpiar.setOnClickListener { limpiarCampos() }

        // Botón para salir al login
        btnRegresar.setOnClickListener { confirmarSalida() }
    }

    private fun validarCampos(): Boolean {
        var camposCompletos = true
        if (txtNumRecibo.text.toString().isEmpty()) {
            txtNumRecibo.error = "Campo obligatorio"
            camposCompletos = false
        }
        if (txtNombre.text.toString().isEmpty()) {
            txtNombre.error = "Campo obligatorio"
            camposCompletos = false
        }
        if (txtHorasT.text.toString().isEmpty()) {
            txtHorasT.error = "Campo obligatorio"
            camposCompletos = false
        }
        if (txtHorasE.text.toString().isEmpty()) {
            txtHorasE.error = "Campo obligatorio"
            camposCompletos = false
        }
        if (radioGroup.checkedRadioButtonId == -1) {
            Toast.makeText(this, "Por favor, selecciona una opción", Toast.LENGTH_SHORT).show()
            camposCompletos = false
        }
        return camposCompletos
    }

    private fun calcularNomina() {
        val horasT = txtHorasT.text.toString().toInt()
        val horasE = txtHorasE.text.toString().toInt()
        val puesto = obtenerPuestoSeleccionado()
        val subtotal = ReciboNomina.calcularSubtotal(horasT, horasE, puesto)
        val impuesto = ReciboNomina.calcularImpuesto(subtotal)
        val total = ReciboNomina.calcularTotal(subtotal, impuesto)
        txtSubtotal.text = subtotal.toString()
        txtImpuesto.text = impuesto.toString()
        txtTotal.text = total.toString()
    }

    private fun obtenerPuestoSeleccionado(): Int {
        var puestoSeleccionado = 0
        if (radioAuxiliar.isChecked) {
            puestoSeleccionado = 1
        } else if (radioAlbañil.isChecked) {
            puestoSeleccionado = 2
        } else if (radioIngObra.isChecked) {
            puestoSeleccionado = 3
        }
        return puestoSeleccionado
    }

    private fun limpiarCampos() {
        txtNumRecibo.setText("")
        txtNombre.setText("")
        txtHorasT.setText("")
        txtHorasE.setText("")
        txtSubtotal.text = ""
        txtImpuesto.text = ""
        txtTotal.text = ""
        radioGroup.clearCheck()
    }

    private fun confirmarSalida() {
        val builder = AlertDialog.Builder(this@ReciboNominaActivity)
        builder.setTitle("Regresar")
        builder.setMessage("¿Estás seguro de que quieres regresar?")
        builder.setPositiveButton(
            "Sí"
        ) { dialog, which ->
            finish() // Cerrar la actividad actual y salir de la aplicación
        }
        builder.setNegativeButton("No", null) // No hacer nada si se selecciona "No"
        val dialog = builder.create()
        dialog.show()
    }

    class ReciboNomina {
        companion object {
            fun calcularSubtotal(horasT: Int, horasE: Int, puesto: Int): Double {
                // Lógica para calcular el subtotal
                // Aquí puedes implementar tu propio cálculo según las reglas de tu aplicación
                return horasT * getPrecioHoraRegular(puesto) + horasE * getPrecioHoraExtra(puesto)
            }

            fun calcularImpuesto(subtotal: Double): Double {
                // Lógica para calcular el impuesto
                // Aquí puedes implementar tu propio cálculo según las reglas de tu aplicación
                return subtotal * 0.1 // Ejemplo: impuesto del 10% sobre el subtotal
            }

            fun calcularTotal(subtotal: Double, impuesto: Double): Double {
                // Lógica para calcular el total
                // Aquí puedes implementar tu propio cálculo según las reglas de tu aplicación
                return subtotal + impuesto
            }

            private fun getPrecioHoraRegular(puesto: Int): Double {
                // Obtener el precio por hora regular según el puesto
                // Aquí puedes implementar la lógica para obtener el precio correspondiente al puesto
                return when (puesto) {
                    1 -> 10.0 // Ejemplo: precio por hora regular para Auxiliar es $10
                    2 -> 15.0 // Ejemplo: precio por hora regular para Albañil es $15
                    3 -> 20.0 // Ejemplo: precio por hora regular para IngObra es $20
                    else -> 0.0 // Valor por defecto si no se encuentra el puesto
                }
            }

            private fun getPrecioHoraExtra(puesto: Int): Double {
                // Obtener el precio por hora extra según el puesto
                // Aquí puedes implementar la lógica para obtener el precio correspondiente al puesto
                return getPrecioHoraRegular(puesto) * 1.5 // Ejemplo: precio por hora extra es 1.5 veces el precio regular
            }
        }
    }

}