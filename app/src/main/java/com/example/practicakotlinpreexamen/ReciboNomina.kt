package com.example.practicakotlinpreexamen
//creacion del proyecto reciboNomina
//terminado
class ReciboNomina {
    private val PAGO_BASE = 200.0
    fun calcularSubtotal(horasNormales: Int, horasExtras: Int, puesto: Int): Double {
        var incrementoPago = 0.0
        if (puesto == 1) {
            incrementoPago = PAGO_BASE * 1.2
        } else if (puesto == 2) {
            incrementoPago = PAGO_BASE * 1.5
        } else if (puesto == 3) {
            incrementoPago = PAGO_BASE * 2.0
        }
        return incrementoPago * horasNormales + incrementoPago * horasExtras * 2
    }

    fun calcularImpuesto(subtotal: Double): Double {
        return subtotal * 0.16
    }

    fun calcularTotal(subtotal: Double, impuesto: Double): Double {
        return subtotal - impuesto
    }
}