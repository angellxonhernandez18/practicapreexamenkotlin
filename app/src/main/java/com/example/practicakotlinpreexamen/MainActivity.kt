package com.example.practicakotlinpreexamen
//Recursos r y values
//terminado
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    private lateinit var txtNombre: EditText
    private lateinit var lblNombre: TextView
    private lateinit var btnEntrar: Button
    private lateinit var btnSalir: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        txtNombre = findViewById(R.id.txtNombre)
        lblNombre = findViewById(R.id.lblNombre)
        btnEntrar = findViewById(R.id.btnEntrar)
        btnSalir = findViewById(R.id.btnSalir)

        btnEntrar.setOnClickListener(View.OnClickListener {
            val usuario = txtNombre.text.toString()
            if (usuario == getString(R.string.user)) {
                // Credenciales válidas, redireccionar a otra actividad
                val intent = Intent(this@MainActivity, ReciboNominaActivity::class.java)
                intent.putExtra("user", usuario) // Agregar el nombre de usuario al intent
                startActivity(intent)
            } else {
                // Credenciales inválidas, mostrar mensaje de error
                txtNombre.error = "Credenciales inválidas"
            }
        })

        btnSalir.setOnClickListener(View.OnClickListener {
            val builder = AlertDialog.Builder(this@MainActivity)
            builder.setTitle("Cerrar aplicación")
            builder.setMessage("¿Estás seguro de que quieres cerrar la aplicación?")
            builder.setPositiveButton(
                "Sí"
            ) { dialog, which ->
                finish() // Cerrar la actividad actual y salir de la aplicación
            }
            builder.setNegativeButton("No", null) // No hacer nada si se selecciona "No"
            val dialog = builder.create()
            dialog.show()
        })
    }
}